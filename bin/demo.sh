#!/bin/bash

if ! which mpstat &> /dev/null
then
    echo 'Could not find mpstat command, cannot operate! (Perhaps you need to install the sysstat package?)'
    exit 1
fi

if ! which awk &> /dev/null
then
    echo 'Could not find awk command, cannot operate! (Should be available via GNU coreutils...)'
    exit 1
fi

if ! which utilbars &> /dev/null
then
    echo 'Could not find utilbars command, cannot operate! (Perhaps you need to add its directory to your PATH?)'
    exit 1
fi

echo 'Estimated CPU usage (100% - idle%):'
mpstat 1 | awk 'NR > 3 {print 100 - $NF" 100"; fflush()}' | utilbars
