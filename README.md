# utilization-bars

Script that accepts list of space-separated numbers, interprets them as value–max pairs, and prints a horizontal utilization bar chart to standard out.

Suitable utilization over time, even in real-time.